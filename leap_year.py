#!/usr/bin/env python3
r"""A simple implementation of a module to tell if a given year is a leap year.
If not year is passed to the module it will default to the current year.

Example usage ``doctest``:

>>> is_leap_year(1800)
1800 is NOT a leap year.
>>> is_leap_year(1900)
1900 is NOT a leap year.
>>> is_leap_year(1903)
1903 is NOT a leap year.
>>> is_leap_year(2000)
2000 IS a leap year
"""
import argparse
import sys

__version__ = '0.1.0'


def is_leap_year(year):
    """
    Function to determine if a given year is a leap year. 
    

    Will default to current year if no value is given.
    """
    if year==None:
        import datetime
        now = datetime.datetime.now()
        year = now.year
    if year%4==0 and not year%100==0 or year%400==0:
        print("%s IS a leap year" % year)
    else:
        print("%s is NOT a leap year." % year)
        
def main(args):
    """
    Parse inputs and run either is_leap_yeah or ``doctest`` on the module
    """
    parser = argparse.ArgumentParser(
        description="Determine if a YEAR is a leap year.")
    parser.add_argument('--version',
        action='version', version=__version__)
    parser.add_argument('year',
        type=int, nargs='?', default=None)
    parser.add_argument('--run_tests',
        action='store_true',
        help='run module tests')


    args = parser.parse_args()

    if args.run_tests:
        import doctest
        doctest.testmod()
    else:
        is_leap_year(args.year)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))